#include <cstdio>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>


const GLint WIDTH = 600;
const GLint HEIGTH = 800;

int main()
{
  if (!glfwInit()) {
    printf("GLFW Initialisation failed");
    glfwTerminate();
    return 1;
      }
  //Set GLFw Version to 3.3

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGTH, "OpenGl", nullptr, nullptr);

  if (!window) {
    printf("GLFW window creation failed");
    glfwTerminate();

    return 1;
      }

  //Get Buffer Size Infrmation
  int bufferwidth, bufferHeight;

  glfwGetFramebufferSize(window, &bufferwidth, &bufferHeight);

  //Set context for GLEW to use
  glfwMakeContextCurrent(window);
  glfwCreateWindow(WIDTH, HEIGTH, "OpenGL", nullptr, nullptr);

  //Allow modern extension features
  glewExperimental = GL_TRUE;

  if (glewInit() != GLEW_OK) {
    printf("Glew initialisation failed");
    glfwDestroyWindow(window);
    glfwTerminate();
    return 1;
      }

  //Setup Viewport size
  glViewport(0, 0, bufferwidth, bufferHeight);

  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glfwSwapBuffers(window);
      }
  return 0;
}
