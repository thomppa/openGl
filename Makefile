CFLAGS = -std=c++20 -O2
LDFLAGS = -lglfw -lGLEW -lGL -lX11 -LXxf86vm -LXrandr -LXi

all: main.cpp
	g++ $(CFLAGS) -o Main main.cpp $(LDFLAGS)

.PHONY: test clean

test: test
	./Main

clean:
	rm -f Main
